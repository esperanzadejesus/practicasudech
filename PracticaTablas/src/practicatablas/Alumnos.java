package practicatablas;
public class Alumnos{
    private int control;
    private String nombre;
  
    public Alumnos(){
        
    }

    public Alumnos(int control, String nombre) {
        this.control = control;
        this.nombre = nombre;
    }

    /**
     * @return the control
     */
    public int getControl() {
        return control;
    }

    /**
     * @param control the control to set
     */
    public void setControl(int control) {
        this.control = control;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
    

